using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Tutor_Books.Data;
using Tutor_Books.Models;

namespace Tutor_Books.Pages.BooksCategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly Tutor_Books.Data.Tutor_BooksContext _context;

        public IndexModel(Tutor_Books.Data.Tutor_BooksContext context)
        {
            _context = context;
        }

        public IList<BooksCategory> BooksCategory { get;set; }

        public async Task OnGetAsync()
        {
            BooksCategory = await _context.BooksCategory.ToListAsync();
        }
    }
}
