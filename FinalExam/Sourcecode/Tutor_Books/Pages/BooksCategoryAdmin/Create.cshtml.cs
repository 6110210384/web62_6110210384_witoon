using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Tutor_Books.Data;
using Tutor_Books.Models;

namespace Tutor_Books.Pages.BooksCategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly Tutor_Books.Data.Tutor_BooksContext _context;

        public CreateModel(Tutor_Books.Data.Tutor_BooksContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public BooksCategory BooksCategory { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.BooksCategory.Add(BooksCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}