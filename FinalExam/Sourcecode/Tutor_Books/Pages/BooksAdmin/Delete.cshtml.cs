using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Tutor_Books.Data;
using Tutor_Books.Models;

namespace Tutor_Books.Pages_BooksAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly Tutor_Books.Data.Tutor_BooksContext _context;

        public DeleteModel(Tutor_Books.Data.Tutor_BooksContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Books Books { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Books = await _context.BooksList
                .Include(b => b.BooksCat).FirstOrDefaultAsync(m => m.BooksID == id);

            if (Books == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Books = await _context.BooksList.FindAsync(id);

            if (Books != null)
            {
                _context.BooksList.Remove(Books);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
