using Tutor_Books.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Tutor_Books.Data
{
    public class Tutor_BooksContext : IdentityDbContext<NewsUser>
    {
        public DbSet<Books> BooksList { get; set; }
        public DbSet<BooksCategory> BooksCategory { get; set; }
    
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Tutor_Books.db");
        }
    }
}